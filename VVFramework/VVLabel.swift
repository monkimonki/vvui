//
//  VVLabel.swift
//  VVFramework
//
//  Created by mm7 on 01.08.14.
//  Copyright (c) 2014 mm7. All rights reserved.
//

import Foundation
import UIKit
class VVLabel : UILabel{
//    - (void)drawTextInRect:(CGRect)rect {
//    UIEdgeInsets insets = {0, 5, 0, 5};
//    [super drawTextInRect:UIEdgeInsetsInsetRect(rect, insets)];
//    }
    
    
    var leftOffset:CGFloat = 0
    
    override func drawTextInRect(rect: CGRect){
        
        var edgeInset:UIEdgeInsets = UIEdgeInsetsMake(0, leftOffset, 0, 0)
        super.drawTextInRect(UIEdgeInsetsInsetRect(rect, edgeInset))
    }
    
}