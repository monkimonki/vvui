//
//  VVTextFieldLeftLabel.swift
//  VVFramework
//
//  Created by mm7 on 01.08.14.
//  Copyright (c) 2014 mm7. All rights reserved.
//

import Foundation
import UIKit


extension VVTextField{
    

    
    
    var leftLabel: VVLabel? {
        set(newVal){
        
            self.leftView=newVal;
        
        }
        get {
        
            return leftView as? VVLabel
        }
    }
    
    
    
    func updateFrame(){
        var labelFrame:CGRect = self.leftLabel!.frame
        
        labelFrame.size.width=(self.defaultLeftLabelWidth > 0 ? self.defaultLeftLabelWidth :self.frame.size.width/2)
        
        labelFrame =  UIView.alligmentFrame(aligmentType: self.leftLabelAligment, frameToAligment: labelFrame, size: CGSizeMake(labelFrame.size.width, self.frame.size.height));
        
        
        
        self.leftLabel!.frame=labelFrame
    }
    
    
    func textForLeftLabel(aText text:String){
        
        if leftView {
            if self.leftLabel? {
                self.leftLabel!.text=text
                self.leftLabel!.sizeToFit()
                
    
                updateFrame();
            }
            
            
            
      
        }else{
            
            self.createLeftLabel(aText: text);
        }
        
        
    }
    
    
    
    func createLeftLabel(aText text:String){
        
        
    }
    
    
    
}