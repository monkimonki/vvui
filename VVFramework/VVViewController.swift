//
//  VVViewController.swift
//  VVFramework
//
//  Created by mm7 on 01.08.14.
//  Copyright (c) 2014 mm7. All rights reserved.
//

import UIKit

class VVViewController: UIViewController , VVTextFieldDelegate {

    
    var keyboardShown:Bool = false
    var curResponder:UIView?
    var basePosition:CGPoint=CGPointZero
    
    
    class func textFieldsDelegatesArray()->NSMutableArray{
        
        struct Static {
            static var instance :NSMutableArray? = nil
            static var token : dispatch_once_t = 0
        }
        
        dispatch_once(&Static.token) { Static.instance = NSMutableArray() }
        
        return Static.instance!
        
    }
    
    
}
