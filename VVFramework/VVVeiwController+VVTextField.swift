//
//  VVVeiwController+VVTextField.swift
//  VVFramework
//
//  Created by mm7 on 01.08.14.
//  Copyright (c) 2014 mm7. All rights reserved.
//

import Foundation
import UIKit

extension VVViewController {
  
    
    
    
    
    
    func textFieldDelegateAdded(){
      
        if !VVViewController.textFieldsDelegatesArray().containsObject(self) {
            VVViewController.textFieldsDelegatesArray().addObject(self)
            
            NSNotificationCenter.defaultCenter().addObserver(self, selector: "keyboardWillShow:", name:UIKeyboardWillShowNotification, object: nil)
            NSNotificationCenter.defaultCenter().addObserver(self, selector: "keyboardWillHide:", name:UIKeyboardWillHideNotification, object: nil)
            
            NSNotificationCenter.defaultCenter().addObserver(self, selector: "keyboardWillChangeFrame:", name:UIKeyboardWillChangeFrameNotification, object: nil)
            
            
            
        }
        
        
    }
    
    
    
    
    
    
    
    
    
    //MARK : KEYBOARD NOTIFICATION
    
    
    
    func keyboardWillShow(notification:NSNotification){
        
        
        if !self.keyboardShown {
            
            self.basePosition=self.view.center
            self.keyboardShown=true
        var info = notification.userInfo
        let keyboardSize = (info[UIKeyboardFrameBeginUserInfoKey] as NSValue).CGRectValue().size
        
        println("will show keyboard size:")

               updateControllerWithKeyboardSize(keyboardSize)
            
            
            
            
        }
        
    }
    
    func keyboardWillHide(notification:NSNotification){
        
    }
    func keyboardWillChangeFrame(notification:NSNotification){
        
        
          if self.keyboardShown {
        var info = notification.userInfo
        let keyboardSize = (info[UIKeyboardFrameEndUserInfoKey] as NSValue).CGRectValue().size
        
        println("will change frame :")
 
            updateControllerWithKeyboardSize(keyboardSize)
            
            
            
        }
    }
    
    
    
    
    func updateControllerWithKeyboardSize(keyboardSize : CGSize){
        
        println(keyboardSize)
        
        
        
        let appDelegate = UIApplication.sharedApplication().delegate as? AppDelegate
        
        let keyBoardLastY = appDelegate!.window!.frame.size.height-keyboardSize.height
        
        let visableRectFrame:CGRect = appDelegate!.window!.convertRect(self.curResponder!.frame, fromView: self.curResponder?.superview)
        
        
        println("visable rect:")
        println(visableRectFrame)
        let offset = visableRectFrame.origin.y+visableRectFrame.size.height - keyBoardLastY + 10
        
     
        
        println("need to move by")
        println(offset)
        
        
        var newCenter:CGPoint = CGPointMake(self.view.center.x, self.view.center.y-offset)

        if(newCenter.y < self.basePosition.y){
            UIView.animateWithDuration(0.33, animations: {
                self.view.center=newCenter
                
                })
        }
    
        
//        [UIView animateWithDuration:0.33 animations:^{
//            [l.listTableView setContentOffset:CGPointMake(l.listTableView.contentOffset.x, l.listTableView.contentOffset.y+offset) animated:NO];
//            }completion:^(BOOL finished) {
//            }]
//        
        
    }
    
    
    //MARK : TEXTFIELD DELEGATE
    
    func vvTextFieldWillBeginEditing(aTextField: VVTextField)  {
        
        self.curResponder=aTextField;
       
    }
    
    func vvTextFieldShouldReturnButtonPressed() {
        
        self.curResponder?.resignFirstResponder()
        
        UIView.animateWithDuration(0.33, animations: {
            self.view.center=self.basePosition
            
            })
        
        
    }
    
 
    
     
    
}


