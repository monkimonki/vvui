//
//  VVTextField+BaseDelegate.swift
//  VVFramework
//
//  Created by mm7 on 01.08.14.
//  Copyright (c) 2014 mm7. All rights reserved.
//

import Foundation


extension VVTextField{
    
    
    func textFieldShouldBeginEditing(textField: VVTextField!) -> Bool  {
      self.vvDelegate.vvTextFieldWillBeginEditing(textField)
        
        return true
    }
    
    func textFieldShouldReturn(textField: VVTextField!) -> Bool {
        
        self.vvDelegate.vvTextFieldShouldReturnButtonPressed()
        
        return  true
    }
    
}