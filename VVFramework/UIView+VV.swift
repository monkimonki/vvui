//
//  UIView+VV.swift
//  VVFramework
//
//  Created by mm7 on 01.08.14.
//  Copyright (c) 2014 mm7. All rights reserved.
//

import Foundation
import UIKit
enum VVViewAligments{
    case topLeft
    case topMiddle
    case topRight
    case centerLeft
    case centerMiddle
    case centerRight
    case botLeft
    case botMiddle
    case botRight
}

extension UIView{
    
    class func alligmentFrame(aligmentType type:VVViewAligments ,frameToAligment frame:CGRect, size :CGSize)->CGRect{
        
        
        
        var viewFrame:CGRect = frame
        
        switch type {
            
            case .topLeft:
            viewFrame.origin.x=0
            viewFrame.origin.y=0
            case .topMiddle:
            viewFrame.origin.x=size.width/2-viewFrame.size.width/2
            viewFrame.origin.y=0
            case .topRight:
            viewFrame.origin.x=size.width-viewFrame.size.width
            viewFrame.origin.y=0
            
        case .centerLeft:
            viewFrame.origin.x=0
            viewFrame.origin.y=size.height/2-viewFrame.size.height/2
        case .centerMiddle:
            viewFrame.origin.x=size.width/2-viewFrame.size.width/2
            viewFrame.origin.y=size.height/2-viewFrame.size.height/2
        case .centerRight:
            viewFrame.origin.x=size.width-viewFrame.size.width
            viewFrame.origin.y=size.height/2-viewFrame.size.height/2
            
            
        case .botLeft:
            viewFrame.origin.x=0
            viewFrame.origin.y=size.height-viewFrame.size.height
        case .botMiddle:
            viewFrame.origin.x=size.width/2-viewFrame.size.width/2
            viewFrame.origin.y=size.height-viewFrame.size.height
        case .botRight:
            viewFrame.origin.x=size.width-viewFrame.size.width
            viewFrame.origin.y=size.height-viewFrame.size.height
            

            default:
                println("undifined aligment")

        }
        
        
        
        
        return viewFrame
        
        
        
    }
    
    
}