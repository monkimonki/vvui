//
//  VVTextFieldDelegate.swift
//  VVFramework
//
//  Created by mm7 on 01.08.14.
//  Copyright (c) 2014 mm7. All rights reserved.
//

import Foundation



protocol VVTextFieldDelegate {
    
//    var vvTextField:VVTextField {get}
    
      func vvTextFieldWillBeginEditing(aTextField: VVTextField)
    
     func vvTextFieldShouldReturnButtonPressed()
    
    
    
}