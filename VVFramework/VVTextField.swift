//
//  VVTextField.swift
//  VVFramework
//
//  Created by mm7 on 01.08.14.
//  Copyright (c) 2014 mm7. All rights reserved.
//

import UIKit



class VVTextField: UITextField,UITextFieldDelegate {

    
    //MARK : INIT+SETUP
    init(frame: CGRect) {
        super.init(frame: frame)
        setup();
        
        // Initialization code
    }
    
    init(coder aDecoder: NSCoder!){
        super.init(coder: aDecoder)
        setup();
    }

    
    func setCustomLeftView(customView:UIView){
        
    }
    func setup(){
    
    self.delegate=self;
    
    }
    
    //END
    
    //MARK : PROPERTYS
    
    var vvDelegate:VVTextFieldDelegate!{
    
    didSet{
    (self.vvDelegate as VVViewController).textFieldDelegateAdded()
    }
    
    }
    
    
    var defaultLeftLabelWidth:CGFloat = 0;
    var leftLabelLeftOffset:CGFloat = 0;
    var leftLabelAligment:VVViewAligments  = VVViewAligments.centerLeft {
        didSet {
         updateFrame()
        }
    
    }
    //MARK : ovverride properys
//    
//    override  var leftView:UIView!{
//        set {
//
//        self.leftView=newValue;
//        
//        }
//    
//        get{
//        return self.leftView;
//        }
//    }
    
    
    
   // MARK : rectForViews
    
    override func leftViewRectForBounds(bounds: CGRect) -> CGRect {
   
        return leftView!.frame;
        
    }
    
    
    //MARK : SETTING FUNC
    
    
    func createLeftLabel(text:NSString!){
        
    }
    
    
    
    //MARK : TEXT FIELD DELEGATE

    
    
    /*
    // Only override drawRect: if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func drawRect(rect: CGRect)
    {
        // Drawing code
    }
    */

     
  
    
}

